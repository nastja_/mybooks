from django.shortcuts import get_object_or_404, render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.urls import reverse
from .models import BookList, BookGenre, Book
from bs4 import BeautifulSoup
import requests
from django.db.models import Q


def index(request):
    listen = BookList.objects.all()
    return render(request, 'mybooksapp/index.html', {'listen':listen})

def detail_list(request, booklist_id):
    liste = get_object_or_404(BookList, pk=booklist_id)
    genres = BookGenre.objects.all()
    return render(request, 'mybooksapp/detail_list.html', {'liste':liste, 'genres':genres})

def detail_genre(request, bookgenre_id):
    genre = get_object_or_404(BookGenre, pk=bookgenre_id)
    books = Book.objects.filter(genre=bookgenre_id)
    return render(request, 'mybooksapp/detail_genre.html', {'genre':genre, 'books':books})

def detail_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    return render(request, 'mybooksapp/detail_book.html', {'book':book})

def get_thalia_data(thalia_id):
    # https://www.thalia.de/shop/home/artikeldetails/ID147880463.html
    url = f"https://www.thalia.de/shop/home/artikeldetails/ID{thalia_id}.html"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    data = {}
    data["cover"] = soup.select_one("img.largeImg").attrs["src"]
    data["title"] = soup.select_one("h1.ncTitle").string.strip()
    data["author"] = soup.select_one("p.aim-author").get_text().strip()
    data["content"] = soup.select_one("div.beschreibung").get_text().strip()
    data["rating"] = len(soup.select(".main .rating-star.active"))
    euro = soup.select_one(".mainPrice .oPriceLeft").string.strip()
    cent = soup.select_one(".mainPrice .oPriceRight").string.strip()
    data["price"] = float(f"{euro}.{cent}")
    print(f"Data: {data}")
    return data

def get_thalia_id(isbn):
    # https://www.thalia.de/suche?filterPATHROOT=&sq=9783426281550

    # Schritt 1: Wir schauen, ob es ein Buch zur ISBN gibt 
    # und wie die Thalia ID zu dem Buch ist.
    url = f"https://www.thalia.de/suche?filterPATHROOT=&sq={isbn}"
    response = requests.get(url)
    soup = BeautifulSoup(response.text, 'html.parser')
    result = soup.select_one("li.suchergebnis")
    if result == None:
        print(f"Keine Ergebnisse bei Thalia für ISBN {isbn}")
        return None
    id = result.attrs["data-id"]
    print(f"ID für ISBN {isbn}: {id}")
    return id
 

def new_book(request):
    data = {}
    if request.method == 'POST':
        # Formular zur manuellen Eingabe
        if "anlegen" in request.POST:
            title = request.POST["title"]
            author = request.POST["author"]
            content = request.POST["content"]
            rating = request.POST["rating"]
            price = request.POST["price"]
            cover = request.POST["cover"]
            booklist = BookList.objects.get(pk=request.POST["booklist"])
            genre = BookGenre.objects.get(pk=request.POST["genre"])
            Book.objects.create(title=title,
                                author=author,
                                content=content,
                                rating=rating,
                                price=price,
                                cover=cover,
                                booklist=booklist,
                                genre=genre,)
            return HttpResponseRedirect(reverse('mybooksapp:detail_list', args=[booklist.id]))
        if "isbnsuche" in request.POST:
            thalia_id = get_thalia_id(request.POST["isbn"])
            if thalia_id:
                data = get_thalia_data(thalia_id)
    
    return render(request, 'mybooksapp/new_book.html', {"booklists": BookList.objects.all(), "genres": BookGenre.objects.all(), "data": data})


def delete_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    book.delete()
    return HttpResponseRedirect(reverse('mybooksapp:index'))

def update_book_def(book, post):
    if "book_title" in post:
        book.title = post["book_title"]
    if "book_author" in post:
        book.author = post["book_author"]
    if "book_content" in post:
        book.content = post["book_content"]    
    if "book_rating" in post:
        try:
            rating = int(post["book_rating"]) 
        except ValueError:
            rating = 0
        book.rating = rating
    book.done = "book_done" in post
    if "book_price" in post:
        try:
            price = float(post["book_price"]) 
        except ValueError:
            price = 0
        book.price = price
    if "book_cover" in post:
        book.cover = post["book_cover"]
    if "book_booklist" in post:
        book.booklist = get_object_or_404(BookList, pk=post["book_booklist"])
    if "book_genre" in post:
        book.genre = get_object_or_404(BookGenre, pk=post["book_genre"])
    book.save()


def update_book(request, book_id):
    book = get_object_or_404(Book, pk=book_id)
    if request.POST:
        update_book_def(book, request.POST)
        return HttpResponseRedirect(reverse('mybooksapp:detail_book', args=[book.id]))
    listen = BookList.objects.all()
    genres = BookGenre.objects.all()
    context = {
            "listen": listen, 
            "genres": genres, 
            "book": book, 
            }
    return render(request, "mybooksapp/update_book_def.html", context)

def search(request):
    print(request.POST)
    if request.POST:
        search_word = request.POST.get("search").lower()
        books_data = {}
        if search_word == "":
            results = []
        else:
            if request.POST.get("category") == "all":
                results = Book.objects.filter(Q(title__contains=search_word) | Q(author__contains=search_word) | Q(content__contains=search_word)).all()
            elif request.POST.get("category") == "title":
                results = Book.objects.filter(title__contains=search_word).all()
            elif request.POST.get("category") == "author":
                results = Book.objects.filter(author__contains=search_word).all()
            elif request.POST.get("category") == "script":
                results = Book.objects.filter(content__contains=search_word).all()
            
            if not results:
                url = f"https://www.thalia.de/suche?filterPATHROOT=&sq={search_word}"
                response = requests.get(url)
                soup = BeautifulSoup(response.text, 'html.parser')
                number = 0
                for link in soup.find_all("li", {"class":"suchergebnis"}):
                    print(link)
                    if number < 12:
                        books_data[link.select_one("h3").string.strip()] = [link.select_one("img").attrs["src"], ("https://www.thalia.de/"+link.select_one("a").attrs["href"])]
                        number += 1

                print(books_data)

    return render(request, "mybooksapp/search.html", {"search_word": search_word, "results": results, "books_data":books_data})


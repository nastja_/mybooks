from django.contrib import admin
from django.urls import path, include
from . import views

app_name='mybooksapp'
urlpatterns = [
    path('', views.index, name='index'),
    path('<int:booklist_id>/', views.detail_list, name='detail_list'),
    path('detail_genre/<int:bookgenre_id>/', views.detail_genre, name='detail_genre'),
    path('detail_book/<int:book_id>/', views.detail_book, name='detail_book'),
    path('new_book/', views.new_book, name='new_book'),
    path('delete_book/<int:book_id>/', views.delete_book, name='delete_book'),
    path('update_book/<int:book_id>', views.update_book, name='update_book'),
    path('search', views.search, name='search'),
]
